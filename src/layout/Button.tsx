import * as React from "react";
import Icon from "./Icon";

const determineButtonClass = (props: IButtonProps) => {
  let cls = "button";
  if (props.vcentered) {
    cls += " vcentered-button";
  }
  if (props.isDanger) {
    cls += " is-danger";
  } else if (props.isInfo) {
    cls += " is-info";
  } else if (props.isSuccess) {
    cls += " is-success";
  }
  return cls;
};

const Button = (props: IButtonProps) => (
  <button
    className={determineButtonClass(props)}
    onClick={props.onClick as any}
  >
    {props.icon ? <Icon name={props.icon} /> : props.name}
  </button>
);

interface IButtonProps {
  isDanger?: boolean;
  isInfo?: boolean;
  isSuccess?: boolean;
  icon?: string;
  label?: string;
  name?: string;
  onClick: any;
  vcentered?: boolean;
}
export default Button;
