import * as React from 'react'

const headerCreator = (item: any) => (
    <th key={item}>{item}</th>
)

const Table = (props: ITableProps) => (
    <div className="content table-wrapper">
      <table className="table">
        <thead>
          <tr>{ props.keys.map(title => headerCreator(title)) }</tr>
        </thead>
        <tbody>
            { props.items.map(item => props.rowCreator(item)) }
        </tbody>
      </table>
    </div>
)

interface ITableProps {
    keys: [string];
    rowCreator: (item: any) => React.ReactNode;
    items: [any];
}

export default Table
