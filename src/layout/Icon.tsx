import * as React from "react";

function determineIconClass(props: IIconProps) {
  let cls = "icon";
  if (props.isDanger) {
    cls += " has-text-danger";
  } else if (props.isInfo) {
    cls += " has-text-info";
  } else if (props.isSuccess) {
    cls += " has-text-success";
  }
  return cls;
}

const Icon = (props: IIconProps) => (
  <span
    onClick={props.onClick}
    className={
      props.className ? `icon ${props.className}` : determineIconClass(props)
    }
  >
    <i className={props.name} />
  </span>
);

interface IIconProps {
  name: string;
  className?: string;
  isDanger?: boolean;
  isSuccess?: boolean;
  isInfo?: boolean;
  onClick?: any;
}

export default Icon;
