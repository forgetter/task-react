import * as React from 'react'

const Site = (props: {children: React.ReactNode}) => (
  <div className="site">
		{props.children}
  </div>
)

export default Site
