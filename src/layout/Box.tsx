import * as React from 'react'

const Box = (props: IBoxProps) => (
  <div className="box">{props.children}</div>
)

interface IBoxProps {
  children: [React.ReactNode];
}

export default Box
