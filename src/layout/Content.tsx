import * as React from 'react'

const Content = (props: IContentProps) => (
  <div className="site-content">
		{props.children}
  </div>
)

interface IContentProps {
  children: React.ReactNode;
}

export default Content
