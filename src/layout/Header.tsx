import * as React from "react";

import Icon from "./Icon";

const NavTitle = () => (
  <div className="navbar-item">
    <Icon name="fas fa-clipboard-check" />
    Tasks
  </div>
);

const NavBurger = ({ onClick }: { onClick: any }) => (
  <button className="button navbar-burger" onClick={onClick}>
    <span />
    <span />
    <span />
  </button>
);

class Header extends React.Component<any, { isActive: boolean }> {
  constructor(props: any) {
    super(props);
    this.state = {
      isActive: false
    };
  }

  public render() {
    return (
      <nav className="navbar is-info has-shadow">
        <div className="navbar-brand">
          <NavTitle />
          <NavBurger onClick={this.toggleNav} />
        </div>
        <div
          className={
            this.state.isActive ? "navbar-menu is-active" : "navbar-menu"
          }
        >
          <div className="navbar-start" />
          <div className="navbar-end" />
        </div>
      </nav>
    );
  }

  private toggleNav = () => {
    this.setState((prevState: any) => ({
      isActive: !prevState.isActive
    }));
  };
}

export default Header;
