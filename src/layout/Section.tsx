import * as React from 'react'

const Section = ({ children, title }: {children: React.ReactNode; title: string}) => (
    <section className="section">
        <div className="container">
            <h1 className="title">{title}</h1>
            {children}
        </div>
    </section>
)

export default Section