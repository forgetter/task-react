import * as React from "react";

class Select extends React.Component<ISelectProps> {
  public render() {
    return (
      <div className="select">
        <select
          value={this.props.selected && this.props.selected.id}
          onChange={this.onChange}
        >
          {this.props.items.map(item => (
            <option key={item.id} value={item.id}>
              {item.name}
            </option>
          ))}
        </select>
      </div>
    );
  }

  private onChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedId = e.target.value;
    const selectedItem = this.props.items.find(item => item.id === selectedId);
    this.props.onChange(selectedItem);
  };
}

interface ISelectProps {
  items: [any];
  onChange: (item: any) => void;
  selected?: any;
}

export default Select;
