import * as React from "react";
import * as ReactDOM from "react-dom";
import "./layout/index.css";

import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import App from "./core/App";
import registerServiceWorker from "./core/registerServiceWorker";
import reduxStore from "./redux/reduxStore";

ReactDOM.render(
  <Provider store={reduxStore}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById("root") as HTMLElement
);
registerServiceWorker();
