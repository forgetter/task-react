import { combineReducers } from 'redux'
import tasks from '../task/TaskReducer'

export default combineReducers({
    tasks
})