import * as React from 'react'

import { Route, Switch } from 'react-router-dom'
import TasksList from '../task/TasksListContainer'

const Router = () => (
  <Switch>
    <Route exact={true} path='/' component={TasksList}/>
  </Switch>
)

// TODO: not found

export default Router
