import * as React from "react"
import Router from "../core/Router";

import Content from "../layout/Content";
import Footer from "../layout/Footer";
import Header from "../layout/Header";
import Site from "../layout/Site";

const App = () => (
  <Site>
    <Header />
    <Content>
      <Router />
    </Content>
    <Footer />
  </Site>
);

export default App;
