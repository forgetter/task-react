import * as React from "react";
import Icon from "../layout/Icon";
import TaskEdit from "./TaskEdit";

class NewTask extends React.Component<{ onAdd: (name: string) => void }, { edit: boolean }> {
  constructor(props: { onAdd: (name: string) => void }) {
    super(props);
    this.state = {
      edit: false
    };
  }

  public render() {
    return (
      <article className="todo-container">
        <div className="level todo-new is-mobile" onClick={this.startEditMode}>
          <Icon
            className="level-item has-text-info"
            name="fas fa-3x fa-plus-circle"
          />
          {this.renderOverlay()}
        </div>
      </article>
    );
  }

  private startEditMode = () => {
    if (!this.state.edit) {
      this.setState({ edit: true });
    }
  };

  private endEditMode = () => {
    this.setState({ edit: false });
  };

  private addNewTask = (name: string) => {
    this.props.onAdd(name);
    this.endEditMode();
  };

  private renderOverlay = () => {
    if (this.state.edit) {
      return (
        <div className="todo-overlay">
          <TaskEdit onSuccess={this.addNewTask} onCancel={this.endEditMode} />
        </div>
      );
    }
    return null;
  };
}

export default NewTask;
