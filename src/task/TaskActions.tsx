export const ADD_TASK_ACTION = "ADD_TASK_ACTION";
export const EDIT_TASK_ACTION = "EDIT_TASK_ACTION";
export const TASK_OPTIONS_ACTION = "TASK_OPTIONS_ACTION";
export const TASK_DONE_ACTION = "TASK_DONE_ACTION";
export const TASK_REMOVE_ACTION = "TASK_REMOVE_ACTION";

export const addTaskAction = (taskName: string) => {
  return {
    taskName,
    type: ADD_TASK_ACTION
  };
};

export const editTaskAction = (id: number, taskName: string) => {
  return {
    id,
    taskName,
    type: EDIT_TASK_ACTION,
  };
};

export const taskOptionsAction = (id: number, open: boolean) => {
  return {
    id,
    open,
    type: TASK_OPTIONS_ACTION,
  };
};

export const taskDoneAction = (id: number) => {
  return {
    id,
    type: TASK_DONE_ACTION,
  };
};

export const taskRemoveAction = (id: number) => {
  return {
    id,
    type: TASK_REMOVE_ACTION,
  };
};
