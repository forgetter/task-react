import * as React from "react";
import Button from "../layout/Button";

const TaskButtons = ({
  onDone,
  onEdit,
  onRemove
}: {
  onDone: () => void;
  onEdit: () => void;
  onRemove: () => void;
}) => (
  <div className="columns todo-buttons">
    <div className="column is-narrow">
      <Button onClick={onDone} isSuccess={true} vcentered={true} icon="fas fa-check" />
    </div>
    <div className="column is-narrow">
      <Button onClick={onEdit} isInfo={true} vcentered={true} icon="fas fa-edit" />
    </div>
    <div className="column is-narrow">
      <Button onClick={onRemove} isDanger={true} vcentered={true} icon="fas fa-trash" />
    </div>
  </div>
);

export default TaskButtons;
