import * as React from "react";
import Section from "../layout/Section";
import NewTask from "./NewTask";
import Task from "./Task";

const TasksContainer = (props: { children: React.ReactNode }) => (
  <div className="columns is-multiline">{props.children}</div>
);

const mapTasksToComponents = ({
  tasks,
  openOptions,
  removeTask,
  markDone,
  editTask
}: ITaskListProps) => {
  return tasks.map(t => {
    const onClick = (open: boolean) => openOptions(t.id, open);
    const onDone = () => markDone(t.id);
    const onEdit = (name: string) => editTask(t.id, name);
    const onRemove = () => removeTask(t.id);
    return (
      <Task
        key={t.id}
        name={t.name}
        done={t.done}
        optionsOpened={t.optionsOpened}
        onClick={onClick}
        markDone={onDone}
        editTask={onEdit}
        removeTask={onRemove}
      />
    );
  });
};

const TasksList = (props: ITaskListProps) => (
  <Section title="Checklist">
    <TasksContainer>
      <NewTask onAdd={props.addTask} />
      {mapTasksToComponents(props)}
    </TasksContainer>
  </Section>
);

interface ITaskListProps {
  tasks: [any];
  addTask: (name: string) => void;
  openOptions: (id: number, open: boolean) => void;
  removeTask: (id: number) => void;
  markDone: (id: number) => void;
  editTask: (id: number, name: string) => void;
}

export default TasksList;
