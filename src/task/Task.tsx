import * as React from 'react';
import TaskButtons from './TaskButtons';
import TaskEdit from './TaskEdit';

class Task extends React.Component<ITaskProps, {edit: boolean}> {

  constructor(props: ITaskProps) {
    super(props);
    this.state = {
      edit: false
    }
  }

  public render() {
    return (
      <article className="todo-container">
        <div className={this.props.done ? "todo-done" : "todo"} onClick={this.toggleOptions}>
          <h2 className="subtitle">{this.props.name}</h2>
          {this.renderOverlay()}
        </div>
      </article>
    )
  }

  private onSuccess = (name: string) => {
    this.props.editTask(name); 
    this.setState({edit: false})
  }

  private onCancel = () => this.setState({edit: false})

  private onEdit = () => this.setState({edit: true})

  private renderOverlay = () => (
    <div className="todo-overlay">
      {this.state.edit 
        ? <TaskEdit value={this.props.name}
          onSuccess={this.onSuccess}
          onCancel={this.onCancel}
        /> 
        : this.props.optionsOpened 
          ? <TaskButtons 
            onEdit={this.onEdit} 
            onDone={this.props.markDone}
            onRemove={this.props.removeTask}
          /> 
          : null}
    </div>
  )

  private toggleOptions = () => {
    this.props.onClick(!this.props.optionsOpened)
  }
}

interface ITaskProps {
  name: string;
  done: boolean;
  optionsOpened: boolean;
  onClick: (opened: boolean) => void;
  editTask: (edit: string) => void;
  markDone: () => void;
  removeTask: () => void;
}

export default Task