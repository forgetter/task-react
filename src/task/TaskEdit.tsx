import * as React from "react";
import Button from "../layout/Button";

class TaskEdit extends React.Component<ITaskEditTypes, { value: string }> {
  constructor(props: ITaskEditTypes) {
    super(props);
    this.state = {
      value: props.value || ""
    };
  }

  public render() {
    return (
      <div className="todo-edit">
        <div className="todo-textarea-container">
          <textarea
            className="textarea todo-textarea"
            placeholder="Your task description"
            value={this.state.value}
            onChange={this.onChange}
          />
        </div>
        <div className="columns todo-edit-buttons is-mobile">
          <div className="column is-narrow">
            <Button isSuccess={true} icon="fas fa-check" onClick={this.onSuccess} />
          </div>
          <div className="column is-narrow">
            <Button
              isDanger={true}
              icon="fas fa-times"
              onClick={this.props.onCancel}
            />
          </div>
        </div>
      </div>
    );
  }

  private onSuccess = () => this.props.onSuccess(this.state.value);

  private onChange = (e: any) => this.setState({ value: e.target.value });
}

interface ITaskEditTypes {
  onSuccess: (value: string) => void;
  onCancel: () => void;
  value?: string;
}

export default TaskEdit;
