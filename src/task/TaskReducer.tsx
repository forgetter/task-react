import * as Action from "./TaskActions";

let idCounter = 4;

const initTasks = [
  {
    id: 1,
    name: "lorum",
    optionsOpened: false
  },
  {
    id: 2,
    name: "costam",
    optionsOpened: false
  },
  {
    id: 3,
    name: "sipsum",
    optionsOpened: false
  }
];

const tasks = (state = initTasks, action: any) => {
  switch (action.type) {
    case Action.ADD_TASK_ACTION:
      return [
        ...state,
        {
          done: false,
          id: idCounter++,
          name: action.taskName,
          optionsOpened: false,
        }
      ];
    case Action.TASK_OPTIONS_ACTION:
      return state.map(t => {
        t.optionsOpened = action.open && t.id === action.id;
        return t;
      });
    case Action.TASK_DONE_ACTION:
      return state.map((t: any) => {
        t.done = t.id === action.id;
        return t;
      });
    case Action.TASK_REMOVE_ACTION:
      return state.filter(t => t.id !== action.id);
    case Action.EDIT_TASK_ACTION:
      return state.map(t => {
        if (t.id === action.id) {
          t.name = action.taskName;
        }
        return t;
      });
    default:
      return state;
  }
};

export default tasks;
