import { connect } from 'react-redux'
import * as Action from './TaskActions'
import TasksList from './TasksList'

const mapStateToProps = (state: any) => ({
  tasks: state.tasks,
})

const mapDispatchToProps: any = (dispatch: (action: object) => void) => ({
  addTask: (task: string) => dispatch(Action.addTaskAction(task)),
  editTask: (id: number, taskName: string) => dispatch(Action.editTaskAction(id, taskName)),
  markDone: (id: number) => dispatch(Action.taskDoneAction(id)),
  openOptions: (id: number, open: boolean) => dispatch(Action.taskOptionsAction(id, open)),
  removeTask: (id: number) => dispatch(Action.taskRemoveAction(id)),
})
 
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TasksList)
